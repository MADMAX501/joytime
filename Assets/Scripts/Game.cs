﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class Game : MonoBehaviour
{

    public Text answer, TextGor;
    public Image Banner, ANSWERPANEL, krest;
    public Button St, Like, More, uznatnow, menu, like2, _2018, btnTest, back;
    private string s, d1, d2, d3, k1, k2, k3, LoadPred;
    public GameObject znaki;
    public ScrollRect gorockops, textovichok, tests;
    int Type;
    int gor;
    public Text textik;
    int NumberZnak;
    int sum = 0;
    int fad = 0;

    //========================
    public QuestionList[] questionsPast, questionsNum, questionsExtra, questionsAge, questionsAngel, questionUbedit, questionsKonf, questionsHappy, questionsDepr;
    QuestionList crntQ;
    public Text qText, ResultText;
    public Text countText;
    public Text[] answersText;
    public Button[] answerBttns = new Button[3];
    List<object> qList;
    int count = 0;
    int Test;
    int GGG;
    bool rewardState = false;
    public Button adv;
    //========================

    public string[] obsh;
    public string[] love;
    public string[] zdorov;
    public string[] fin;
    public string[] work;

    public string[] Prediction;

    System.Random r = new System.Random();

    static System.DateTime date1;

    BannerView bannerView;
    InterstitialAd interstitial;
    string IdBanner = "ca-app-pub-9481177103858825/8756394941";
    string IdOb = "ca-app-pub-9481177103858825/8864206153";
    string adUnitId = "ca-app-pub-9481177103858825/7606964805";
    string appId = "ca-app-pub-9481177103858825~3396257052";
    private RewardBasedVideoAd rewardBaseVideoAd;
    public Button privacy;


    public Text TextFactUp, fact, countFact;
    public Button vpered, nazad, menufromFact;
    public TextAsset[] fcts;
    List<string> ListFacts;
    int cntFcts;

    public void addd()
    {
        
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.joy.time.goroskop");
    }

    public void Policy()
    {
        Application.OpenURL("http://joytimegame.xyz/");
    }
    public void Ob()
    {
        interstitial = new InterstitialAd(IdOb);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
    }

    public void ObView()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

    public void RequestBanner()
    {
        bannerView = new BannerView(IdBanner, AdSize.SmartBanner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
    }

    private void LoadRewardBaseAd()
    {
        rewardBaseVideoAd.LoadAd(new AdRequest.Builder().Build(), adUnitId);
    }

    private void ShowRewardBasedAd()
    {
        if (rewardBaseVideoAd.IsLoaded())
        {
            rewardBaseVideoAd.Show();
        }
    }


    private void Update()
    {
        if (rewardState == true)
        {
            BackRolic();
            rewardState = false;
        }
    }

    private void Start()
    {
        //if(Application.platform != RuntimePlatform.Android)
        MobileAds.Initialize(appId);
        RequestBanner();
        Ob();
        rewardBaseVideoAd = RewardBasedVideoAd.Instance;

        rewardBaseVideoAd.OnAdClosed += HandleOnAdClosed;
        rewardBaseVideoAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        rewardBaseVideoAd.OnAdLeavingApplication += HandleOnAdLeavingApplication;
        rewardBaseVideoAd.OnAdLoaded += HandleOnAdLoaded;
        rewardBaseVideoAd.OnAdOpening += HandleOnAdOpening;
        rewardBaseVideoAd.OnAdRewarded += HandleOnAdRewarded;
        rewardBaseVideoAd.OnAdStarted += HandleOnAdStarted;
        LoadRewardBaseAd();
        T();

        if (System.Convert.ToInt32(d1) != PlayerPrefs.GetInt("d1") || System.Convert.ToInt32(d2) != PlayerPrefs.GetInt("d2") || System.Convert.ToInt32(d3) != PlayerPrefs.GetInt("d3"))
        {
            for (int i = 0; i < 12; i++)
            {
                PlayerPrefs.SetInt("state" + i, 0);
            }
        }
        // PlayerPrefs.DeleteAll();
    }

    public void CloseMenu()
    {
        Banner.gameObject.SetActive(false);
        adv.gameObject.SetActive(false);
        St.gameObject.SetActive(false);
        Like.gameObject.SetActive(false);
        krest.gameObject.SetActive(false);
        privacy.gameObject.SetActive(false);
        More.gameObject.SetActive(false);
        btnTest.gameObject.SetActive(false);
        Type = 0;
        back.gameObject.SetActive(true);
        znaki.gameObject.SetActive(true);
        textik.gameObject.SetActive(true);
        textik.text = "Твой знак зодиака:";
        _2018.gameObject.SetActive(false);    }

    public void CloseMenu1()
    {
        Banner.gameObject.SetActive(false);
        adv.gameObject.SetActive(false);
        St.gameObject.SetActive(false);
        Like.gameObject.SetActive(false);
        krest.gameObject.SetActive(false);
        privacy.gameObject.SetActive(false);
        More.gameObject.SetActive(false);
        btnTest.gameObject.SetActive(false);
        Type = 1;
        back.gameObject.SetActive(true);
        gorockops.gameObject.SetActive(true);
        textik.text = "Выбери горскоп на 2019:";
        textik.gameObject.SetActive(true);
        _2018.gameObject.SetActive(false);
    }

    public void menuGorockop(int i)
    {
        gorockops.gameObject.SetActive(false);
        textik.text = "Твой знак зодиака:";
        znaki.gameObject.SetActive(true);
        gor = i;
    }

    //===============ТЕСТЫ===============================================

    public void Test_Begin()
    {
        Type = 2;
        back.gameObject.SetActive(true);
        Banner.gameObject.SetActive(false);
        adv.gameObject.SetActive(false);
        St.gameObject.SetActive(false);
        Like.gameObject.SetActive(false);
        krest.gameObject.SetActive(false);
        privacy.gameObject.SetActive(false);
        More.gameObject.SetActive(false);
        btnTest.gameObject.SetActive(false);
        _2018.gameObject.SetActive(false);
        textik.text = "Выбери тест:";
        textik.gameObject.SetActive(true);
        tests.gameObject.SetActive(true);
    }
    public void Past()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 0;
        qList = new List<object>(questionsPast);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }
    public void Num()
    {
        back.gameObject.SetActive(false);
        tests.gameObject.SetActive(false);
        textik.gameObject.SetActive(true);
        textik.text = "Твой знак зодиака:";
        znaki.gameObject.SetActive(true);
    }
    public void Extra()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 2;
        qList = new List<object>(questionsExtra);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }
    public void Age()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 3;
        qList = new List<object>(questionsAge);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }
    public void Angel()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 4;
        qList = new List<object>(questionsAngel);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }
    public void Ubedit()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 5;
        qList = new List<object>(questionUbedit);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }
    public void Konf()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 6;
        qList = new List<object>(questionsKonf);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }
    public void Happy()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 7;
        qList = new List<object>(questionsHappy);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }
    public void Depr()
    {
        back.gameObject.SetActive(false);
        countText.gameObject.SetActive(true);
        textik.gameObject.SetActive(false);
        Test = 8;
        qList = new List<object>(questionsDepr);
        tests.gameObject.SetActive(false);
        questionGenerate();
    }

    void questionGenerate()
    {
        int l;
        if (Test == 0 || Test == 5)
            l = 10;
        else if (Test == 1)
            l = 14;
        else if (Test == 6 || Test == 7)
            l = 15;
        else
            l = 20;
        if (count < l)
        {
            crntQ = qList[count] as QuestionList;
            StartCoroutine(animBttns());
            StartCoroutine(appearTxt());
        }
        else
        {
            ObView();
            Ob();
            proverka();
            count = 0;
            StartCoroutine(closeT());
            qText.gameObject.SetActive(false);
            countText.text = "";
            StartCoroutine(appearName());

        }
    }


    public void FactAnswer()
    {
        if (NumberZnak == 0)
        {
            TextFactUp.text = "ФАКТЫ ОБ ОВНАХ:";
        }
        else if (NumberZnak == 1)
        {
            TextFactUp.text = "ФАКТЫ О ТЕЛЬЦАХ:";
        }
        else if (NumberZnak == 2)
        {
            TextFactUp.text = "ФАКТЫ О БЛИЗНЕЦАХ:";
        }
        else if (NumberZnak == 3)
        {
            TextFactUp.text = "ФАКТЫ О РАКАХ:";
        }
        else if (NumberZnak == 4)
        {
            TextFactUp.text = "ФАКТЫ О ЛЬВАХ:";
        }
        else if (NumberZnak == 5)
        {
            TextFactUp.text = "ФАКТЫ О ДЕВАХ:";
        }
        else if (NumberZnak == 6)
        {
            TextFactUp.text = "ФАКТЫ О ВЕСАХ:";
        }
        else if (NumberZnak == 7)
        {
            TextFactUp.text = "ФАКТЫ О СКОРПИОНАХ:";
        }
        else if (NumberZnak == 8)
        {
            TextFactUp.text = "ФАКТЫ О СТРЕЛЬЦАХ:";
        }
        else if (NumberZnak == 9)
        {
            TextFactUp.text = "ФАКТЫ О КОЗЕРОГАХ:";
        }
        else if (NumberZnak == 10)
        {
            TextFactUp.text = "ФАКТЫ О ВОДОЛЕЯХ:";
        }
        else if (NumberZnak == 11)
        {
            TextFactUp.text = "ФАКТЫ О РЫБАХ:";
        }
        ListFacts = new List<string>(fcts[NumberZnak].text.Split('\n'));
        fact.text = ListFacts[0];
        cntFcts = 0;
        countFact.text = (cntFcts + 1) + "/" + ListFacts.Count;
    }

    public void BtnVpered()
    {
        fad++;
        if (fad % 8 == 0)
        {
            ObView();
            Ob();
        }

        if (cntFcts == 0)
            nazad.gameObject.SetActive(true);
        if (cntFcts == ListFacts.Count - 2)
            vpered.gameObject.SetActive(false);

        cntFcts++;
        fact.text = ListFacts[cntFcts];
        countFact.text = (cntFcts + 1) + "/" + ListFacts.Count;
    }

    public void BtnNazad()
    {
        if (cntFcts == 1)
            nazad.gameObject.SetActive(false);
        if (cntFcts == ListFacts.Count - 1)
            vpered.gameObject.SetActive(true);

        cntFcts--;
        fact.text = ListFacts[cntFcts];
        countFact.text = (cntFcts + 1) + "/" + ListFacts.Count;
    }

    public void openFacts()
    {
        Type = 5;
        Banner.gameObject.SetActive(false);
        adv.gameObject.SetActive(false);
        St.gameObject.SetActive(false);
        Like.gameObject.SetActive(false);
        krest.gameObject.SetActive(false);
        privacy.gameObject.SetActive(false);
        More.gameObject.SetActive(false);
        btnTest.gameObject.SetActive(false);
        back.gameObject.SetActive(true);
        znaki.gameObject.SetActive(true);
        textik.gameObject.SetActive(true);
        textik.text = "Твой знак зодиака:";
        _2018.gameObject.SetActive(false);
    }


    void proverka()
    {
        if (Test == 0)
        {
            int g = r.Next(1, 6) - 1;

            textik.text = "Результат:";
            if (g == 0)
                ResultText.text = "В прошлой жизни вы были великим ученым и привнесли большой вклад в развитие технических наук.";
            else if (g == 1)
                ResultText.text = "В прошлой жизни вы были царем или императором и управляли огромными территориями.";
            else if (g == 2)
                ResultText.text = "В прошлой жизни вы были моделью, которую многие люди копируют по сегодняшний день.";
            else if (g == 3)
                ResultText.text = "В прошлой жизни вы были воином или самураем  и погибли как храбрец в бою.";
            else if (g == 4)
                ResultText.text = "В прошлой жизни вы были великим поэтом, книги и стихи которого известны во всем мире.";
            else if (g == 5)
                ResultText.text = "В прошлой жизни вы были магом, могли творить чудеса и предсказывать будущее.";
        }
        else if (Test == 1)
        {
            if (GGG == 0)
            {
                textik.text = "Ваши счастливые числа: 7 и 9";
                ResultText.text = "Семерка дарует независимость, духовность, аналитические способности. Ее обладатели отстранены от влияния других людей, а сила числа помогает достигать успеха ценой своих усилий, ума, изобретательности и высокой интуиции.Число девять наделяет его обладателей интуицией, чувствительностью, мощной энергетикой, силой духа. У таких людей наблюдается тяга ко всему неизвестному и неведомому.  ";
            }
            else if (GGG == 1)
            {
                textik.text = "Ваши счастливые числа:2 и 4";
                ResultText.text = "Двойка помогает раскрыть в себе дипломатические способности, таланты, высокие духовные качества. Четверка символизирует безграничный ум, верность, наблюдательность, усидчивость, самостоятельность. Хозяевам числа 4 можно безоговорочно доверять, они несомненно протянут вам руку помощи. ";
            }
            else if (GGG == 2)
            {
                textik.text = "Ваши счастливые числа: 3 и 4";
                ResultText.text = "О силе цифры три слагали легенды еще в древние времена. Даже в православии тройка играет важную роль: не зря практически все молитвы мы произносим трижды. Четверка символизирует безграничный ум, верность, наблюдательность, усидчивость, самостоятельность. Хозяевам числа 4 можно безоговорочно доверять, они несомненно протянут вам руку помощи.";
            }
            else if (GGG == 3)
            {
                textik.text = "Ваши счастливые числа: 5 и 8";
                ResultText.text = "Родившиеся под управлением пятерки наделены энергичностью, природным магнетизмом и большими возможностями. Число 5 одаривает стабильностью, успешностью и тягой к самопознанию. Восьмерка обладает огромным энергетическим потенциалом и одаряет человека целеустремленностью, энергичностью и философским мышлением. ";
            }
            else if (GGG == 4)
            {
                textik.text = "Ваши счастливые числа: 3 и 8";
                ResultText.text = "О силе цифры три слагали легенды еще в древние времена. Даже в православии тройка играет важную роль: не зря практически все молитвы мы произносим трижды. Восьмерка обладает огромным энергетическим потенциалом и одаряет человека целеустремленностью, энергичностью и философским мышлением. ";
            }
            else if (GGG == 5)
            {
                textik.text = "Ваши счастливые числа: 3 и 7";
                ResultText.text = "О силе цифры три слагали легенды еще в древние времена. Даже в православии тройка играет важную роль: не зря практически все молитвы мы произносим трижды. Это символ святости, веры, созидания. Семерка дарует независимость, духовность, аналитические способности. Ее обладатели отстранены от влияния других людей, а сила числа помогает достигать успеха ценой своих усилий, ума, изобретательности и высокой интуиции. ";
            }
            else if (GGG == 6)
            {
                textik.text = "Ваши счастливые числа: 2 и 6";
                ResultText.text = "Двойка помогает раскрыть в себе дипломатические способности, таланты, высокие духовные качества. Поистине добрые и отзывчивые люди рождаются под эгидой цифры 2 — миротворцы, которым под силу разрешить любой спор. Шестерка характеризует долголетие, молодость души, харизматичность, обаяние и престиж среди людей. Рожденные под ее покровительством легко добиваются расположения людей, что играет на руку в обретении высокого положения в обществе. ";
            }
            else if (GGG == 7)
            {
                textik.text = "Ваши счастливые числа: 5 и 9";
                ResultText.text = "Родившиеся под управлением пятерки наделены энергичностью, природным магнетизмом и большими возможностями. Число 5 одаривает стабильностью, успешностью и тягой к самопознанию. Число девять наделяет его обладателей интуицией, чувствительностью, мощной энергетикой, силой духа.";
            }
            else if (GGG == 8)
            {
                textik.text = "Ваши счастливые числа: 3 и 8";
                ResultText.text = "О силе цифры три слагали легенды еще в древние времена. Даже в православии тройка играет важную роль: не зря практически все молитвы мы произносим трижды. Это символ святости, веры, созидания. Восьмерка обладает огромным энергетическим потенциалом и одаряет человека целеустремленностью, энергичностью и философским мышлением. ";
            }
            else if (GGG == 9)
            {
                textik.text = "Ваши счастливые числа: 5 и 8";
                ResultText.text = "Родившиеся под управлением пятерки наделены энергичностью, природным магнетизмом и большими возможностями. Число 5 одаривает стабильностью, успешностью и тягой к самопознанию. Восьмерка обладает огромным энергетическим потенциалом и одаряет человека целеустремленностью, энергичностью и философским мышлением. ";
            }
            else if (GGG == 10)
            {
                textik.text = "Ваши счастливые числа: 1 и 3";
                ResultText.text = "Единица символизирует индивидуальность, целеустремленность, лидерство. Цифра наделяет человека неограниченными возможностями для достижения поставленных целей. О силе цифры три слагали легенды еще в древние времена. Даже в православии тройка играет важную роль: не зря практически все молитвы мы произносим трижды.";
            }
            else if (GGG == 11)
            {
                textik.text = "Ваши счастливые числа: 6 и 7";
                ResultText.text = "Шестерка характеризует долголетие, молодость души, харизматичность, обаяние и престиж среди людей. Рожденные под ее покровительством легко добиваются расположения людей, что играет на руку в обретении высокого положения в обществе. Семерка дарует независимость, духовность, аналитические способности. Ее обладатели отстранены от влияния других людей, а сила числа помогает достигать успеха ценой своих усилий, ума, изобретательности и высокой интуиции.";
            }
        }
        else if (Test == 2)
        {
            textik.text = "Результат";
            ResultText.text = "Ты определенно необычный человек! Видеть будущее во снах или предугадывать события - обычное дело для тебя. Развивай свои способности, возможно, ты действительно экстрасенс!";
        }
        else if (Test == 3)
        {
            int u = r.Next(18, 45);
            textik.text = "Твой психологический возраст:";
            ResultText.text = u + "";
        }
        else if (Test == 4)
        {
            textik.text = "Результат";
            ResultText.text = "Вы - ангел! Вы стремитесь к справедливости. Честь для вас превыше всего, а помощь людям важная часть вашей жизни. Отлично, так держать!";
        }
        else if (Test == 5)
        {
            textik.text = "Результат";
            if (sum < 7)
                ResultText.text = "Вас очень легко убедить. Вы привыкли доверять людям, особенно в принятии решений, в которых вы совершенно не разбираетесь. Вам легче поверить  знающим людям, чем терять время, чтобы разобраться в вопросе. Но вам нужно быть предельно осторожным, так как некоторые люди однажды захотят воспользоваться Вашим доверием.";
            else if (sum >= 7 && sum < 14)
                ResultText.text = "Вас нелегко убедить. Вы очень предусмотрительны и расчетливы. Но Вас легко смутить и заставить сомневаться. Хотя иногда это Вас наоборот помогает при принятии верных решений в трудных вопросах.  Вы из тех типов людей, которые послушают совет и сделают по-своему.";
            else
                ResultText.text = "Вас невозможно убедить. Вы всегда все делаете по-своему, но при этом Вы очень упрямы и совершенно не можете воспринимать критику в вашу сторону. Иногда Вам стоит прислушаться к мнению других людей, иначе Ваше упрямство может навредить. ";
        }
        else if (Test == 6)
        {
            textik.text = "Результат";
            if (sum <= 10)
                ResultText.text = "Вас можно назвать очень тактичным и абсолютно не конфликтным человеком. Вы предпочитаете все конфликтные ситуации улаживать с помощью переговоров. Вас можно назвать приятным для окружающих человеком, который всегда готов прийти на помощь, если потребуется. Для Вас очень важно не потерять уважения в глазах друзей и родных.";
            else if (sum > 10 && sum <= 20)
                ResultText.text = "Вас можно назвать в меру конфликтным человеком, потому что если Вы видите, что где то с Вами поступили несправедливо, то обязательно выскажетесь по этому поводу. Даже если на карту поставлена Ваша карьера, Вы не смолчите и будете до конца отстаивать свою точку зрения. За это Вас уважают окружающие.";
            else
                ResultText.text = "Вы обладаете разрушительной энергией. Малейшая критика в Вашу сторону или неосторожно сказанное слово грозит сказавшему крупными неприятностями. Вы способны одним взглядом «испепелить» своего противника. Остается только догадываться, на что Вы способны. Многие люди Вас побаиваются, а те, кто знает о Вашем характере, стараются держаться от Вас подальше и лишний раз не попадаться Вам на глаза. Такими темпами Вы можете растерять тех друзей, которые у Вас еще остались!";
        }
        else if (Test == 7)
        {
            textik.text = "Результат";
            if (sum <= 10)
                ResultText.text = "Вы очень счастливый человек, никто и ничто не может испортить вам жизнь. вы во всем видите позитив, от вас излучается невидимая добрая энергия. вы своим позитивным настроением можете вытащить любого человека из депрессии, поэтому старайтесь не давать унывать своим друзьям и близким. ";
            else
                ResultText.text = "Вы счастливый человек, но иногда вам что-то мешает. возможно это напряженная работа или учеба. вам больше времени нужно уделять себе и добавить в вашу жизнь активный отдых. ";
        }
        else if (Test == 8)
        {
            textik.text = "Результат";
            if (sum <= 14)
                ResultText.text = "У вас нет депрессии, хоть в вашей жизни и бывают неудачи и трудности, вы легко с ними справляетесь. Вы легко можете находить общий язык даже с незнакомыми вам людьми. Вашему позитивному мышлению можно позавидовать.";
            else
                ResultText.text = "У вас умеренная депрессия,  вы и сами это прекрасно ощущаете. Возможно, это связанно с эмоциональным перенапряжением на работе, учебе или недопониманием в кругу родных. Для того чтобы депрессия отступила, вам необходимо расслабиться и больше времени проводить в кругу друзей или заниматься любимыми делами. Лучше всего от депрессии помогает активный отдых.";
        }
    }

    IEnumerator appearTxt()
    {
        yield return new WaitForSeconds(0.3f);
        qText.text = crntQ.question;
        qText.gameObject.SetActive(true);
        if (Test == 0 || Test == 5)
            countText.text = (count + 1) + "/10";
        else if (Test == 1)
            countText.text = (count + 1) + "/14";
        else if (Test == 6 || Test == 7)
        {
            countText.text = (count + 1) + "/15";
        }
        else
            countText.text = (count + 1) + "/20";
    }
    IEnumerator closeT()
    {
        for (int b = 0; b < 3; b++)
        {
            answerBttns[b].interactable = false;
            answerBttns[b].gameObject.SetActive(false);
            qText.gameObject.SetActive(false);
        }
        yield return new WaitForSeconds(0.3f);
    }

    IEnumerator animBttns()
    {
        if (count == 0)
            yield return new WaitForSeconds(1f);
        else
            yield return new WaitForSeconds(0.3f);
        for (int i = 0; i < crntQ.answers.Length; i++)
            answersText[i].text = crntQ.answers[i];
        int a = 0;
        while (a < answerBttns.Length)
        {
            if (!answerBttns[a].gameObject.activeSelf)
                answerBttns[a].gameObject.SetActive(true);
            else
                answerBttns[a].gameObject.GetComponent<Animator>().SetTrigger("In");
            a++;
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < answerBttns.Length; i++)
            answerBttns[i].interactable = true;
        yield break;
    }

    IEnumerator appearName()
    {
        yield return new WaitForSeconds(0.5f);
        textik.gameObject.SetActive(true);
        ResultText.gameObject.SetActive(true);
        menu.gameObject.SetActive(true);
        like2.gameObject.SetActive(true);
    }

    public void answersButtons(int index)
    {
        if (Test == 0 || Test == 5)
        {
            if (/*count == 3 || */count == 5)
            {
                ObView();
                Ob();
            }
        }
        else if (Test == 1 || Test == 6 || Test == 7)
        {
            if (/*count == 3 || count == 6 ||*/ count == 4 || count == 9)
            {
                ObView();
                Ob();
            }
        }
        else
        {
            if (count == 5 || count == 10 || count == 15/* || count == 15 || count == 17*/)
            {
                ObView();
                Ob();
            }
        }

        if (Test == 5)
        {
            if (index == 1)
            {
                sum = sum + 1;
            }
            else
            {
                sum = sum + 2;
            }
        }
        if (Test == 6)
        {
            if (index == 1)
            {
                sum = sum + 1;
            }
            else
            {
                sum = sum + 2;
            }
        }
        if (Test == 7)
        {
            if (index == 1)
            {
                sum = sum + 1;
            }
            else
            {
                sum = sum + 2;
            }
        }
        if (Test == 8)
        {
            if (index == 1)
            {
                sum = sum + 1;
            }
            else
            {
                sum = sum + 2;
            }
        }
        count++;
        questionGenerate();
        StartCoroutine(closeT());
    }

    //=============================================================================================

    void AppearAnswer()
    {

        randAns();

        ANSWERPANEL.gameObject.SetActive(true);
        answer.gameObject.SetActive(true);

        uznatnow.gameObject.SetActive(true);
        menu.gameObject.SetActive(true);
        like2.gameObject.SetActive(true);
    }

    void BackMenu()
    {
        ANSWERPANEL.gameObject.SetActive(false);
        answer.gameObject.SetActive(false);
        uznatnow.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);
        like2.gameObject.SetActive(false);
        Banner.gameObject.SetActive(true);
        if (Application.platform == RuntimePlatform.Android)
            adv.gameObject.SetActive(true);
        St.gameObject.SetActive(true);
        Like.gameObject.SetActive(true);
        krest.gameObject.SetActive(true);
        privacy.gameObject.SetActive(true);
        More.gameObject.SetActive(true);
        _2018.gameObject.SetActive(true);
        btnTest.gameObject.SetActive(true);
    }

    void BackMenu2()
    {
        textovichok.gameObject.SetActive(false);
        textik.gameObject.SetActive(false);
        like2.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);
        Banner.gameObject.SetActive(true);
        if (Application.platform == RuntimePlatform.Android)
            adv.gameObject.SetActive(true);
        St.gameObject.SetActive(true);
        Like.gameObject.SetActive(true);
        krest.gameObject.SetActive(true);
        privacy.gameObject.SetActive(true);
        More.gameObject.SetActive(true);
        _2018.gameObject.SetActive(true);
        btnTest.gameObject.SetActive(true);
    }

   

    public void BR()
    {
        BackRolic();
    }
    void BackRolic()
    {
        int k = r.Next(1, Prediction.Length) - 1;

        while (Prediction[k] == PlayerPrefs.GetString("Pred" + NumberZnak))
        {
            k = r.Next(1, Prediction.Length) - 1;
        }

        answer.text = d1 + "/" + d2 + "/" + d3 + "\n" + Prediction[k];
        PlayerPrefs.SetString("Pred" + NumberZnak, Prediction[k]);
        LoadRewardBaseAd();
    }

    public void Back()
    {
        if (Type == 0)
        {
            /*ObView();
            Ob();*/
            BackMenu();
        }
        else if (Type == 1)
        {
            BackMenu2();
        }
        else
        {
            menu.gameObject.SetActive(false);
            like2.gameObject.SetActive(false);
            textik.gameObject.SetActive(false);
            ResultText.gameObject.SetActive(false);
            Banner.gameObject.SetActive(true);
            if (Application.platform == RuntimePlatform.Android)
                adv.gameObject.SetActive(true);
            St.gameObject.SetActive(true);
            Like.gameObject.SetActive(true);
            krest.gameObject.SetActive(true);
            privacy.gameObject.SetActive(true);
            More.gameObject.SetActive(true);
            _2018.gameObject.SetActive(true);
            btnTest.gameObject.SetActive(true);
        }
    }

    
    public void FFF(int i)
    {
        if (Type == 0)
        {
            ObView();
            Ob();
            NumberZnak = i;
            back.gameObject.SetActive(false);
            textik.gameObject.SetActive(false);
            znaki.gameObject.SetActive(false);
            AppearAnswer();
        }
        else if (Type == 1)
        {
            ObView();
            Ob();
            back.gameObject.SetActive(false);
            textik.text = "Твой гороскоп:";
            znaki.gameObject.SetActive(false);
            textovichok.gameObject.SetActive(true);
            menu.gameObject.SetActive(true);
            like2.gameObject.SetActive(true);
            if (gor == 0)
            {
                TextGor.text = obsh[i];
            }
            else if (gor == 1)
            {
                TextGor.text = love[i];
            }
            else if (gor == 2)
            {
                TextGor.text = zdorov[i];
            }
            else if (gor == 3)
            {
                TextGor.text = fin[i];
            }
            else if (gor == 4)
            {
                TextGor.text = work[i];
            }
        }
        else if(Type == 5)
        {
            NumberZnak = i;
            back.gameObject.SetActive(false);
            znaki.gameObject.SetActive(false);
            textik.gameObject.SetActive(false);

            FactAnswer();
            fact.gameObject.SetActive(true);
            TextFactUp.gameObject.SetActive(true);
            countFact.gameObject.SetActive(true);
            vpered.gameObject.SetActive(true);
            menufromFact.gameObject.SetActive(true);
        }
        else
        {

            GGG = i;
            textik.gameObject.SetActive(false);
            znaki.gameObject.SetActive(false);
            countText.gameObject.SetActive(true);
            textik.gameObject.SetActive(false);
            Test = 1;
            qList = new List<object>(questionsNum);
            tests.gameObject.SetActive(false);
            questionGenerate();
        }
    }

    public void nazadFromFact()
    {
        fact.gameObject.SetActive(false);
        TextFactUp.gameObject.SetActive(false);
        countFact.gameObject.SetActive(false);
        vpered.gameObject.SetActive(false);
        nazad.gameObject.SetActive(false);
        menufromFact.gameObject.SetActive(false);
        if (Application.platform == RuntimePlatform.Android)
            adv.gameObject.SetActive(true);
        Banner.gameObject.SetActive(true);
        St.gameObject.SetActive(true);
        Like.gameObject.SetActive(true);
        krest.gameObject.SetActive(true);
        privacy.gameObject.SetActive(true);
        More.gameObject.SetActive(true);
        _2018.gameObject.SetActive(true);
        btnTest.gameObject.SetActive(true);
    }

    public void galka()
    {
        ShowRewardBasedAd();
    }
    public void randAns()
    {
        if (PlayerPrefs.HasKey("d1"))
        {
            if (System.Convert.ToInt32(d1) == PlayerPrefs.GetInt("d1") && System.Convert.ToInt32(d2) == PlayerPrefs.GetInt("d2") && System.Convert.ToInt32(d3) == PlayerPrefs.GetInt("d3"))
            {
                if (PlayerPrefs.HasKey("state" + NumberZnak))
                {
                    if (PlayerPrefs.GetInt("state" + NumberZnak) == 1)
                    {
                        answer.text = d1 + "/" + d2 + "/" + d3 + "\n" + PlayerPrefs.GetString("Pred" + NumberZnak);
                    }
                    else
                    {
                        PlayerPrefs.SetInt("state" + NumberZnak, 1);

                        int k = r.Next(1, Prediction.Length) - 1;

                        while (Prediction[k] == PlayerPrefs.GetString("Pred" + NumberZnak))
                        {
                            k = r.Next(1, Prediction.Length) - 1;
                        }

                        answer.text = d1 + "/" + d2 + "/" + d3 + "\n" + Prediction[k];
                        PlayerPrefs.SetString("Pred" + NumberZnak, Prediction[k]);
                    }
                }
                else
                {
                    PlayerPrefs.SetInt("state" + NumberZnak, 1);

                    int k = r.Next(1, Prediction.Length) - 1;

                    while (Prediction[k] == PlayerPrefs.GetString("Pred" + NumberZnak))
                    {
                        k = r.Next(1, Prediction.Length) - 1;
                    }

                    answer.text = d1 + "/" + d2 + "/" + d3 + "\n" + Prediction[k];
                    PlayerPrefs.SetString("Pred" + NumberZnak, Prediction[k]);
                }
            }
            else
            {
                PlayerPrefs.SetInt("state" + NumberZnak, 1);

                int k = r.Next(1, Prediction.Length) - 1;

                while (Prediction[k] == PlayerPrefs.GetString("Pred" + NumberZnak))
                {
                    k = r.Next(1, Prediction.Length) - 1;
                }

                answer.text = d1 + "/" + d2 + "/" + d3 + "\n" + Prediction[k];
                PlayerPrefs.SetString("Pred" + NumberZnak, Prediction[k]);
                PlayerPrefs.SetInt("d1", System.Convert.ToInt32(d1));
                PlayerPrefs.SetInt("d2", System.Convert.ToInt32(d2));
                PlayerPrefs.SetInt("d3", System.Convert.ToInt32(d3));
                PlayerPrefs.SetString("Pred" + NumberZnak, Prediction[k]);
            }
        }
        else
        {
            PlayerPrefs.SetInt("state" + NumberZnak, 1);
            int k = r.Next(1, Prediction.Length) - 1;
            answer.text = d1 + "/" + d2 + "/" + d3 + "\n" + Prediction[k];
            PlayerPrefs.SetString("Pred" + NumberZnak, Prediction[k]);
            PlayerPrefs.SetInt("d1", System.Convert.ToInt32(d1));
            PlayerPrefs.SetInt("d2", System.Convert.ToInt32(d2));
            PlayerPrefs.SetInt("d3", System.Convert.ToInt32(d3));

        }
    }

    public void BBBack()
    {
        if (Type == 0)
        {
            znaki.gameObject.SetActive(false);
            textik.gameObject.SetActive(false);
            back.gameObject.SetActive(false);
            Banner.gameObject.SetActive(true);
            if (Application.platform == RuntimePlatform.Android)
                adv.gameObject.SetActive(true);
            St.gameObject.SetActive(true);
            Like.gameObject.SetActive(true);
            krest.gameObject.SetActive(true);
            privacy.gameObject.SetActive(true);
            More.gameObject.SetActive(true);
            _2018.gameObject.SetActive(true);
            btnTest.gameObject.SetActive(true);
        }
        else if (Type == 1)
        {
            gorockops.gameObject.SetActive(false);
            textik.gameObject.SetActive(false);
            znaki.gameObject.SetActive(false);
            back.gameObject.SetActive(false);
            Banner.gameObject.SetActive(true);
            if (Application.platform == RuntimePlatform.Android)
                adv.gameObject.SetActive(true);
            St.gameObject.SetActive(true);
            Like.gameObject.SetActive(true);
            krest.gameObject.SetActive(true);
            privacy.gameObject.SetActive(true);
            More.gameObject.SetActive(true);
            _2018.gameObject.SetActive(true);
            btnTest.gameObject.SetActive(true);
        }
        else
        {
            sum = 0;
            textik.gameObject.SetActive(false);
            tests.gameObject.SetActive(false);
            back.gameObject.SetActive(false);
            Banner.gameObject.SetActive(true);
            if (Application.platform == RuntimePlatform.Android)
                adv.gameObject.SetActive(true);
            St.gameObject.SetActive(true);
            Like.gameObject.SetActive(true);
            krest.gameObject.SetActive(true);
            privacy.gameObject.SetActive(true);
            More.gameObject.SetActive(true);
            _2018.gameObject.SetActive(true);
            btnTest.gameObject.SetActive(true);
        }
    }
    


    public void T()
    {
        date1 = System.DateTime.Now;
        s = date1.ToString();

        char[] c = s.ToCharArray();

        d2 = c[0].ToString();

        if (c[1] != '/')
        {
            d2 = d2 + c[1].ToString();
            d1 = c[3].ToString();
            if (c[4] != '/')
            {
                d1 = d1 + c[4].ToString();
                d3 = c[6].ToString();
                d3 = d3 + c[7].ToString();
                d3 = d3 + c[8].ToString();
                d3 = d3 + c[9].ToString();
            }
            else
            {
                d1 = 0 + d1;
                d3 = c[5].ToString();
                d3 = d3 + c[6].ToString();
                d3 = d3 + c[7].ToString();
                d3 = d3 + c[8].ToString();
            }
            print(d2);
        }
        else
        {
            d2 = 0 + d2;
            d1 = c[2].ToString();
            if (c[3] != '/')
            {
                d1 = d1 + c[3].ToString();

                d3 = c[5].ToString();
                d3 = d3 + c[6].ToString();
                d3 = d3 + c[7].ToString();
                d3 = d3 + c[8].ToString();
            }
            else
            {
                d3 = c[4].ToString();
                d3 = d3 + c[5].ToString();
                d3 = d3 + c[6].ToString();
                d3 = d3 + c[7].ToString();
            }
        }


    }

    public void Mores()
    {
        //Application.OpenURL("https://play.google.com/store/apps/developer?id=JoyTime");
    }

    public void Exit()
    {
        Application.Quit();
    }
    public void Likes()
    {
        //Application.OpenURL("https://play.google.com/store/apps/details?id=com.joy.time.future");
    }

    public void HandleOnAdLoaded(object sender, System.EventArgs args)
    {
    }
    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {

    }
    public void HandleOnAdOpening(object sender, System.EventArgs args)
    {
    }
    public void HandleOnAdStarted(object sender, System.EventArgs args)
    {
    }
    public void HandleOnAdClosed(object sender, System.EventArgs args)
    {
    }
    public void HandleOnAdRewarded(object sender, Reward args)
    {
        rewardState = true;
    }
    public void HandleOnAdLeavingApplication(object sender, System.EventArgs args)
    {
    }

}


[System.Serializable]
public class QuestionList
{
    public string question;
    public string[] answers = new string[3];
}
